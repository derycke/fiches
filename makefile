PAGES = getopt makefile debogueur

all: $(PAGES)
	echo '<ul>' > public/index.html
	echo '<li><a href="getopt">getopt</a></li>' >> public/index.html
	echo '<li><a href="makefile">makefile</a></li>' >> public/index.html
	echo '<li><a href="debogueur">debogueur</a></li>' >> public/index.html
	echo '</ul>' >> public/index.html

.PHONY: all $(PAGES)

getopt: public/getopt/index.html

public/getopt/index.html: getopt/getopt.md
	mkdir -p public/getopt
	pandoc -s --output=$@ $<

makefile: public/makefile/index.html

public/makefile/index.html: make/makefile.md
	mkdir -p public/makefile
	pandoc -s --output=$@ $<

debogueur: public/debogueur/index.html

public/debogueur/index.html: debogueur/valgrind_gdb.md
	mkdir -p public/debogueur
	pandoc -s --output=$@ $<
