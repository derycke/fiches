# Makefile

## Cuisine

Un makefile est un fichier décrivant des recettes.

```makefile
gateau: riz lait oeufs sucre vanille
    Mélanger le sucre avec le lait, ajouter la vanille et faire monter en ébullition.
    Rajouter le riz et laisser cuire pendant 20min à feu frémissant.
    Battre les œufs puis les ajouter au mélange.
    Faire cuire au bain marie pendant 45min à 180°C
```

Pour produire le `gateau` il faut avoir du `riz`, du `lait`, des `oeufs` du `sucre` et de la `vanille`. Ce sont des dépendances pour la confection du `gateau`.

On peut décomposer une recette en sous-recette

```makefile
gateau: pate
    Faire cuire au bain marie pendant 45min à 180°C
pate: melange oeufs
    Battre les œufs puis les ajouter au mélange.
melange: lait_chaud riz
    Rajouter le riz et laisser cuire pendant 20min à feu frémissant.
lait_chaud: lait vanille
    Mélanger le sucre avec le lait, ajouter la vanille et faire monter en ébullition.
```

Pour faire la `pate` de mon gateau, j'ai besoin du `mélange` et des `oeufs`

## En pratique

Dans la réalité véritable, on utilise `make` afin de compiler et/ou installer des programmes.
En `C`, on fabrique un exécutable à partir de fichiers sources en utilisant un compilateur (ici `gcc`).

```makefile
mon_programme: un.c deux.c trois.c quatre.h
    gcc -Wall un.c deux.c trois.c -lm -o mon_programme
    # la recette produit bien le programme mon_programme à partir des fichiers indiqués
```

Pour produire ce programme, il suffit alors de lancer la commande 
```shell=
$ make mon_programme # exécute et affiche la ligne suivante
gcc -Wall un.c deux.c trois.c -lm -o mon_programme
```

::: info 
**Qu'est ce qu'on y gagne ?**

Outre la simplicité de la ligne de `make mon_programme`, `make` permet de ne compiler que ce qui est nécessaire en utilisant les dates de modification de fichiers concernés. 
Si `deux.c` change, alors il produira à nouveau `mon_programme`.

```shell
$ make mon_programme
gcc -Wall un.c deux.c trois.c -lm -o mon_programme
$ make mon_programme # rien de nouveau
rien à faire pour « mon_programme »
$ touch deux.c       # on modifie le fichier deux.c
$ make mon_programme # il faut donc recompiler
gcc -Wall un.c deux.c trois.c -lm -o mon_programme
```
:::

## Les sous-recettes

Compiler un programme directement à partir de toutes ses sources est une pratique couteuse en temps et en énergie.
On y préfère la compilation séparée : on compile séparément chaque fichier source, puis on lie le tout en un exécutable.
En appliquant le principe des sous recettes, on obtient le makefile suivant:

```makefile
mon_programme: un.o deux.o trois.o
    gcc un.o deux.o trois.o -lm -o mon_programme
un.o: un.c
    gcc -Wall un.c -c -o un.o
deux.o: deux.c quatre.h
    gcc -Wall deux.c -c -o deux.o
trois.o: trois.c
    gcc -Wall trois.c -c -o trois.o
```

Cela permet entre autre d'affiner les dépendances. Le fichier `deux.c` peut ainsi dépendre de `quatre.h` sans que ça soit le cas pour `un.c` ou `trois.c`.
Donc modifier `quatre.h` n'impose la compilation que de `deux.o` et `mon_programme`.

```shell
$ make mon_programme
gcc -Wall un.c -c -o un.o
gcc -Wall deux.c -c -o deux.o
gcc -Wall trois.c -c -o trois.o
gcc un.o deux.o trois.o -lm -o mon_programme
$ make mon_programme # rien de nouveau
rien à faire pour « mon_programme »
$ touch deux.c       # on modifie le fichier deux.c
$ make mon_programme # on ne recompile que ce qu'il faut
gcc -Wall deux.c -c -o deux.o 
gcc un.o deux.o trois.o -lm -o mon_programme
```

## Des variables

Chaque recette ayant son produit et ses ingrédients, il est possible d'y faire simplement référence à l'aide de variable afin d'éviter les oublis si jamais on décide de rajouter des ingrédients.

On dispose dans chaque recette des variables

 - `$@` qui désigne le produit (par son nom)
 - `$^` la liste des ingrédients
 - `$<` le premier ingrédient

On peut ainsi réécrire notre makefile sous la forme

```makefile
mon_programme: un.o deux.o trois.o
    gcc $^ -lm -o $@
un.o: un.c
    gcc -Wall $< -c -o $@
deux.o: deux.c quatre.h
    gcc -Wall $< -c -o $@
trois.o: trois.c
    gcc -Wall $< -c -o $@
```

On retrouve régulièrement dans les makefile quelques autres variables aux noms standards dont l'utilisation est ciblée.

```makefile
# CFLAGS contient les options de compilation des sources C en binaire.
CFLAGS = -Wall

# LDLIBS contient la liste des bibliothèques nécessaires à la création
# de l'exécutable
LDFLAGS = -lm

# CC correpond au compilateur à utiliser
CC = gcc

mon_programme: un.o deux.o trois.o
    $(CC) $^ $(LDLIBS) -o $@
un.o: un.c
    $(CC) $(CFLAGS) $< -c -o $@
deux.o: deux.c quatre.h
    $(CC) $(CFLAGS) $< -c -o $@
trois.o: trois.c
    $(CC) $(CFLAGS) $< -c -o $@
```

Cela permet entre autres de rajouter simplement des options de compilation qui seront appliquées sur toutes les règles, par exemple l'ajout du `-g` pour les informations de débogage.


## Autres raccourcis

L'exemple précédent fait apparaître des répétitions de `$(CC) $(CFLAGS) $< -c -o $@` (`gcc -Wall $< -c -o $@`) qui transforme chacune un `.o` à partir d'un `.c`.

Dans ce genre de situation, commune, on peut avoir des recettes fonctionnant suitant un motif: pour produire `bidule.o`, je prends `bidule.c`.
On écrit ça de la manière suivante :

```makefile
%.o: %.c
    $(CC) $(CFLAGS) $< -c -o $@
```

Très bien mais pour la dépendance à `quatre.h` ?
Et bien on peut la rajouter de indépendamment de la recette.


```makefile
mon_programme: un.o deux.o trois.o
    $(CC) $^ $(LDLIBS) -o $@
    
# dépendance supplémentaire
deux.o: quatre.h

%.o: %.c
    gcc -Wall $< -c -o $@
```
