---
title: Valgrind et GDB
---

# Un peu de débogage

Voici quelques codes comportant des erreurs plus ou moins grossières et simples à identifier.

En complément de vos connaissances du langage `C`, vous disposez des outils `valgrind` et `gdb` pour traquer les bug lors de l'exécution des programmes.

Ces deux outils sont des outils de débogage. Aussi pour augmenter la qualité de leurs messages, notamment en localisant les informations dans votre code source, il est important d'ajouter l'option de compilation `-g` lors de la compilation des fichiers `.c`.

## Valgrind

`valgrind` est un outil puissant qui se comporte comme une machine virtuelle, émulant l'exécution d'un programme en surveillant l'utilisation de la mémoire et effectuant différente mesure (comme le nombre d'appels de chaque fonction).

Ici, on ne souhaite pas faire du profilage mais s'assurer que les programmes n'utilisent pas de manière aberrante la mémoire et respectent les bonnes pratiques.

### Exemple d'utilisation : dépassement de capacité

Voici un premier programme avec une petite erreur.

```c
#include <stdlib.h>

int main(void) {
    int *tableau = malloc(sizeof(int) * 10);

    for (int i = 0; i <= 10; i++)
        tableau[i] = i;

    free(tableau);

    return 0;
}
```

On le compile avec la commande `gcc -Wall -std=c17 -g exemple.c -o exemple` avant de l'exécuter par l'intermédiaire de `valgrind`.

```
$ valgrind ./exemple
==27466== Memcheck, a memory error detector
==27466== Copyright (C) 2002-2022, and GNU GPL'd, by Julian Seward et al.
==27466== Using Valgrind-3.21.0 and LibVEX; rerun with -h for copyright info
==27466== Command: ./exemple
==27466==
==27466== Invalid write of size 4
==27466==    at 0x10917F: main (main.c:7)
==27466==  Address 0x4a57068 is 0 bytes after a block of size 40 alloc'd
==27466==    at 0x4842794: malloc (in /usr/libexec/valgrind/vgpreload_memcheck-amd64-linux.so)
==27466==    by 0x10915A: main (main.c:4)
==27466==
==27466==
==27466== HEAP SUMMARY:
==27466==     in use at exit: 0 bytes in 0 blocks
==27466==   total heap usage: 1 allocs, 1 frees, 40 bytes allocated
==27466==
==27466== All heap blocks were freed -- no leaks are possible
==27466==
==27466== For lists of detected and suppressed errors, rerun with: -s
==27466== ERROR SUMMARY: 1 errors from 1 contexts (suppressed: 0 from 0)
```

`valgrind` détecte un accès invalide dans la mémoire : `Invalid write of size 4 [...] Address 0x4a57068 is 0 bytes after a block of size 40` et indique les positions de l'accès invalide et de l'allocation de la zone concernée.
  - `Invalid write of size 4` : on écrit 4 octets (un `int` ou un `float`) (pour une lecture, ce serait `read`)
  - `... is 0 bytes after a block of size 40` : l'accès se fait juste après une zone de mémoire allouée de 40 octets.

Sans regarder le code, on peut deviner qu'on manipule un tableau `tab` de 10 éléments de taille 4 octets (ie. `int`, `float`) et qu'on écrit dans la case `tab[10]` qui n'existe pas.

### Second exemple : Libération manquante

Voici le code d'un programme avec peu de libérations :

```c
#include <stdlib.h>

static char *c_decale;
static char *d_copie;

int main(void) {
    char *a = malloc(4);
    free(a);
    a = NULL;

    char *b = malloc(5);
    b = NULL;

    char *c = malloc(7);
    c_decale = c + 4;

    char *d = malloc(13);
    d_copie = d;

    char **e = malloc(sizeof(char*) * 4);
    e[0] = malloc(17);

    return 0;
}
```

que l'on compile avec la commande `gcc -Wall -std=c17 -g mon_programme.c -o mon_programme`.

```
$ valgrind ./mon_programme
==11994== Memcheck, a memory error detector
==11994== Copyright (C) 2002-2022, and GNU GPL'd, by Julian Seward et al.
==11994== Using Valgrind-3.21.0 and LibVEX; rerun with -h for copyright info
==11994== Command: ./mon_programme
==11994==
==11994==
==11994== HEAP SUMMARY:
==11994==     in use at exit: 74 bytes in 5 blocks
==11994==   total heap usage: 6 allocs, 1 frees, 78 bytes allocated
==11994==
==11994== LEAK SUMMARY:
==11994==    definitely lost: 37 bytes in 2 blocks
==11994==    indirectly lost: 17 bytes in 1 blocks
==11994==      possibly lost: 7 bytes in 1 blocks
==11994==    still reachable: 13 bytes in 1 blocks
==11994==         suppressed: 0 bytes in 0 blocks
==11994== Rerun with --leak-check=full to see details of leaked memory
==11994==
==11994== For lists of detected and suppressed errors, rerun with: -s
==11994== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
```

À la fin de l'exécution, `valgrind` fait un récapitulatif de l'utilisation du tas (_HEAP_):

  - `in use at exit: 74 bytes in 5 blocks` : 74 octets alloués par 5 allocations n'ont pas été libéré avant la fin du programme
  - `total heap usage: 6 allocs, 1 frees, 78 bytes allocated` : six allocations (`malloc`), une libération (`free`) et 78 octets alloués au total

Dans le cas où certains blocs alloués ne sont pas libéré, `valgrind` précise le type de _fuite_ mémoire :

  - `definitely lost: 37 bytes in 2 blocks` : l'adresse de la zone mémoire est perdue (probablement dans la pile) (`b` et `e`)
  - `indirectly lost: 17 bytes in 1 blocks` : l'adresse de la zone mémoire est dans une zone mémoire dont l'adresse est perdue (tableau de pointeur) (`e[0]`)
  - `possibly lost: 7 bytes in 1 blocks` : on dispose d'une adresse dans la zone mémoire mais pas au début de cette dernière (`b_decale`)
  - `still reachable: 13 bytes in 1 blocks` : l'adresse de la zone mémoire non libérée est dans la mémoire globale (`c_copie`)

Afin de nous aider à identifier l'origine des fuites, `valgrind` est capable de donner l'emplacement de chaque allocation incriminée dans le code avec l'option `--leak-check=full`.

**Tester avec `valgrind --leak-check=full ./mon_programme`.**

### Résolution des problèmes

Si votre programme comporte des erreurs d'accès invalides, `valgrind` devient rapidement très bavard.
Il ne faut pas pour autant se laisser impressionner et commencer méthodiquement par les premières erreurs.

Concernant les fuites de mémoires, il convient de s'attaquer au type `definitely lost` qui sera le plus commun dans vos codes.
Si vous utilisez une bibliothèque externe (MVL, SDL, X11...) il est possible que certaines fuites ne soient pas de votre ressort.


## GDB (GNU DeBugger)

`gdb` est le debogueur standard sur Linux (et autres systèmes Unix).
Il permet l'analyse de bugs avec, par exemple, l'exécution pas à pas de programmes tout en inspectant le contenu de la mémoire et des registres.

Comme il s'agit d'un outil interactif, sa prise en main est moins aisée que `valgrind` mais on pourra se satisfaire de quelques commandes.

### Exemple d'utilisation : une boucle infinie

Voici un extrait de code présentant une recherche dichotomique.

```c
int dichotomie(int v, int *tab, int size) {
    int i, j;
    int mid;
    i = 0;
    j = size;
    while (i < j) {
        mid = (i + j) / 2;
        if (tab[mid] == v)
            return 1;
        if (tab[mid] < v)
            i = mid;
        else
            j = mid;
    }
    return 0;
}
```

Sur certaines exécutions, on a déterminé que cette fonction ne terminait pas, autrement dit on exécute une boucle infinie.
On prend donc un tel cas (par exemple, on cherche la valeur `3` dans le tableau `{0, 2, 4, 6, 8}`).

Pour lancer `gdb` sur notre programme :

```
$ gdb ./mon_programme
GNU gdb (GDB) 13.2
[bla bla bla de gdb...]
Reading symbols from ./mon_programme...
(gdb) run
```

À ce stade, le programme se comporte comme attendu, avec une boucle infinie.
On peut l'interrompre avec la combinaison `Ctrl C`.

```
(gdb) run
^C
Program received signal SIGINT, Interrupt.
dichotomie (v=3, tab=0x7fffffffdd90, size=5) at mon_programme.c:12
12          if (tab[mid] == v)
```

L'exécution du programme est interrompu mais le programme n'est pas tué pour autant.
On peut reprendre son exécution avec la commande `continue` (et repartir en boucle infinie).

Il est plus intéressant de regarder l'état des variables du programme pour identifier la raison de la boucle infinie.
Deux commandes sont utiles : `print <nom de variable>` et `display <nom de variable>`.
  - `print i` : affiche la valeur de la variable `i` (si elle existe à l'endroit où le programme a été interrompu)
  - `display i` : comme `print i` mais le contenu de la variable est affiché à chaque fois que le programme est interrompu.

On souhaite connaître l'évolution des variables `i`, `j` et `mid` donc on utilise `display`
```
(gdb) display i
1: = 1
(gdb) display j
2: = 2
(gdb) display mid
3: = 1
```

Pour reprendre pas à pas l'exécution, on utilise la commande `next` (ou son diminutif `n`).

```
(gdb) n
10      while (i < j) {
1: i = 1
2: j = 2
3: mid = 1
(gdb) n
11          mid = (i + j) / 2;
1: i = 1
2: j = 2
3: mid = 1
(gdb) n
12          if (tab[mid] == v)
1: i = 1
2: j = 2
3: mid = 1
(gdb) n
14          if (tab[mid] < v)
1: i = 1
2: j = 2
3: mid = 1
(gdb) n
15              i = mid;
1: i = 1
2: j = 2
3: mid = 1
(gdb) n
10      while (i < j) {
1: i = 1
2: j = 2
3: mid = 1
```

Les valeurs de `i`, `j` et `mid` restent inchangées entre les tours de boucles.
On identifie qu'à l'instruction `i = mid`, `i` devrait changé de valeur, donc soit notre calcul de `mid` est incorrect, soit il faut changé notre affectation.

### Autre exemple : plantage hors concours

On dispose d'un code réalisant une liste de mots triés sous la forme d'un tableau

``` {.c .numberLines}
#include <stdio.h>
#include <string.h>

typedef struct {
    char *mots[256];
    int taille;
} Dico;

/* Insertion triée */
void inserer_mot(Dico *dico, char *mot) {
    int i;
    if (dico->taille >= 256)
        return;

    for (i = dico->taille - 1; i >= 0 && strcmp(dico->mots[i], mot) > 0; i--) {
        dico->mots[i + 1] = dico->mots[i];
    }
    dico->mots[i + 1] = mot;

    dico->taille++;
}

void afficher_dico(Dico *dico) {
    int i;
    for (i = 0; i < dico->taille; i++)
        printf("%s\n", dico->mots[i]);
}

int main() {
    Dico dico;
    char mot[256];

    dico.taille = 0;

    /* Mots de base */
    inserer_mot(&dico, "zoo");
    inserer_mot(&dico, "anaconda");
    inserer_mot(&dico, "mitochondrie");

    printf("Dico de base\n"
           "============\n");
    afficher_dico(&dico);
    printf("------------\n");

    /* Ajout de mots de l'utilisateur */
    while (scanf(" %s", mot) > 0) {
        inserer_mot(&dico, mot);
    }

    printf("Dico complet\n"
           "============\n");
    afficher_dico(&dico);
    printf("------------\n");

    return 0;
}
```

Voici le résultat du programme en tapant les mots `serpent` puis `basilic` dans l'entrée standard du programme

```
Dico de base
============
anaconda
mitochondrie
zoo
------------
serpent
basilic
Dico complet
============
anaconda
mitochondrie
basilic
basilic
zoo
------------
```

Le mot `basilic` est mal placé et apparait deux fois, tandis que le mot `serpent` est absent.

Plutôt que d'interrompre maladroitement le programme avec un `Ctrl C`, on peut demander directement à `gdb` de l'interrompre à un emplacement précis.
Ici, les problèmes semble commencer quand on insère un mot entré par l'utilisateur, donc à la ligne 47 (`inserer_mot(&dico, mot);`).

On utilise la commande `break` pour y placer un point d'arrêt (_breakpoint_). Puis on lance le programme.

```gdb
(gdb) break mon_programme.c:47
Breakpoint 1 at 0x12fb: file mon_programme.c, line 47.
(gdb) run
Dico de base
============
anaconda
mitochondrie
zoo
------------
serpent

Breakpoint 1, main () at mon_programme.c:47
47          inserer_mot(&dico, mot);
(gdb)
```

En utilisant la commande `continue` permettant de continuer l'exécution jusqu'au prochain point d'arrêt (et en complétant avec les commandes `next` et `print` pour inspecter le contenu du dictionnaire), voici ce qu'on obtient

```
(gdb) print dico.mots[0]
$1 = 0x555555556008 "anaconda"
(gdb) print dico.mots[0]@4
$2 = {0x555555556008 "anaconda", 0x555555556011 "mitochondrie", 0x7fffffffde40 "serpent", 0x555555556004 "zoo"}
(gdb) continue
basilic

Breakpoint 1, main () at mon_programme.c:47
47          inserer_mot(&dico, mot);
(gdb) print dico.mots[0]@4
$3 = {0x555555556008 "anaconda", 0x555555556011 "mitochondrie", 0x7fffffffde40 "basilic", 0x555555556004 "zoo"}
(gdb) next
(gdb) print dico.mots[0]@5
$4 = {0x555555556008 "anaconda", 0x555555556011 "mitochondrie", 0x7fffffffde40 "basilic", 0x7fffffffde40 "basilic", 0x555555556004 "zoo"}
(gdb) continue
Dico complet
============
anaconda
mitochondrie
basilic
basilic
zoo
------------
[Inferior 1 (process 22214) exited normally]
```

On note que le dictionnaire contient le mot `basilic` avant l'appel à `inserer_mot`.
Les mots `serpent` et `basilic` sont tous deux stockés à l'adresse `0x7fffffffde40` qui est copiée ensuite dans le dictionnaire (deux fois).

_Note_: si dans `print bidule`, `bidule` désigne un élément d'un tableau, `print bidule@4` permet d'afficher `bidule` et les 3 éléments qui le suivent dans le tableau.

## Les sanitizers

Depuis quelques versions, les compilateurs GCC et Clang permettent de faire de l'instrumentation de code à des fins de débogage.
L'instrumentation de code consiste en l'ajout d'instructions machine lors de la compilation, sans modifier son code, permettant ensuite de récolter des mesures et des tests à l'exécution.

Les deux les plus simples à utiliser sont [UBSan](https://clang.llvm.org/docs/UndefinedBehaviorSanitizer.html) et [ASan](https://clang.llvm.org/docs/AddressSanitizer.html).
Le premier détecte des cas de comportements non définis (par exemple, un dépassement de capacité sur un entier).
Le second détecte des lectures et écritures dans des zones de mémoires invalides et les allocations non libérées.

Ces deux outils ne détectent pas tout mais couvrent les erreurs les plus fréquentes.

Pour les utiliser il suffit d'ajouter lors de la compilation et lors de l'édition des liens les options :
- `-fsanitize=undefined` pour UBSan
- `-fsanitize=address` pour ASan
- `-fsanitize=address,undefined` pour les deux

_Note: il est possible d'être plus fin sur ce qu'on souhaite détecter, se référer à la documentation pour plus d'exploration._

Le programme compilé, il suffit de l'utiliser.

Par exemple, sur le programme suivant :

```c
#include <stdlib.h>

int main(int argc, char **argv) {
    (void) argv;
    int *array = malloc(sizeof(int[100]));
    array[0] = 0;
    int res = array[argc + 100]; // BOOM
    free(array);
    return res;
}
```

une fois compilé avec les options `gcc -fsanitize=address,undefined -o main -Wall -std=c17 main.c`,
donne à l'exécution le plantage suivant, provoquer par l'instrumentation de code :

```
=================================================================
==1==ERROR: AddressSanitizer: heap-buffer-overflow on address 0x5140000001d4 at pc 0x0000004012d6 bp 0x7fff913f3510 sp 0x7fff913f3508
READ of size 4 at 0x5140000001d4 thread T0
    #0 0x4012d5 in main main.c:7
    #1 0x76a797a29d8f  (/lib/x86_64-linux-gnu/libc.so.6+0x29d8f) (BuildId: 490fef8403240c91833978d494d39e537409b92e)
    #2 0x76a797a29e3f in __libc_start_main (/lib/x86_64-linux-gnu/libc.so.6+0x29e3f) (BuildId: 490fef8403240c91833978d494d39e537409b92e)
    #3 0x4010f4 in _start (/app/output.s+0x4010f4) (BuildId: 121a517512c3a53cea1dd181e4e00654571de937)

0x5140000001d4 is located 4 bytes after 400-byte region [0x514000000040,0x5140000001d0)
allocated by thread T0 here:
    #0 0x76a7983416e7 in malloc (/opt/compiler-explorer/gcc-14.1.0/lib64/libasan.so.8+0xfb6e7) (BuildId: f2a1ddf12d1777020118f0ad8e8239c309286c0f)
    #1 0x4011cf in main main.c:5
    #2 0x76a797a29d8f  (/lib/x86_64-linux-gnu/libc.so.6+0x29d8f) (BuildId: 490fef8403240c91833978d494d39e537409b92e)

SUMMARY: AddressSanitizer: heap-buffer-overflow main.c:7 in main
Shadow bytes around the buggy address:
  0x513fffffff00: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
  0x513fffffff80: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
  0x514000000000: fa fa fa fa fa fa fa fa 00 00 00 00 00 00 00 00
  0x514000000080: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
  0x514000000100: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
=>0x514000000180: 00 00 00 00 00 00 00 00 00 00[fa]fa fa fa fa fa
  0x514000000200: fa fa fa fa fa fa fa fa fa fa fa fa fa fa fa fa
  0x514000000280: fa fa fa fa fa fa fa fa fa fa fa fa fa fa fa fa
  0x514000000300: fa fa fa fa fa fa fa fa fa fa fa fa fa fa fa fa
  0x514000000380: fa fa fa fa fa fa fa fa fa fa fa fa fa fa fa fa
  0x514000000400: fa fa fa fa fa fa fa fa fa fa fa fa fa fa fa fa
Shadow byte legend (one shadow byte represents 8 application bytes):
  Addressable:           00
  Partially addressable: 01 02 03 04 05 06 07 
  Heap left redzone:       fa
  Freed heap region:       fd
  Stack left redzone:      f1
  Stack mid redzone:       f2
  Stack right redzone:     f3
  Stack after return:      f5
  Stack use after scope:   f8
  Global redzone:          f9
  Global init order:       f6
  Poisoned by user:        f7
  Container overflow:      fc
  Array cookie:            ac
  Intra object redzone:    bb
  ASan internal:           fe
  Left alloca redzone:     ca
  Right alloca redzone:    cb
==1==ABORTING
```

Le message est très verbeux mais l'essentiel de l'information est au début :
`main.c:5:23: runtime error: index 101 out of bounds for type 'int [100]'`.
On accède à un élément hors du tableau.

Vient ensuite la pile d'appel, suivit de l'emplacement de la ligne allouant l'objet probalement concerné dans le cas d'une allocation dynamique.