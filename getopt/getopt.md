# Programmation C -- getopt(3)

```C
/* $ man 3 getopt */
#include <unistd.h>

int getopt(int argc, char * const argv[], const char *optstring);
extern int optind;
extern char *optarg;
```
## Généralités

Les arguments d'un programme sont ceux donnés via la ligne de commande quand on lance un programme. En C, on y accède habituellement via les arguments de la fonction `int main(int argc, char *argv[])`. 

:::info
**Exemple :**
```sh
$ ls -l -a projet "dossier avec espaces"
```
Les 4 arguments donnés à la commande `ls` sont `-l`, `-a`, `projet` et `dossier avec espaces`
:::

Parmis ces arguments, `-l` et `-a` jouent un rôle particulier pour la commande `ls`. Il s'agit d'**options**. On dira que les autres arguments sont des arguments _normaux_.

Pour gérer ces des options, on pourrait écrire le code suivant :

```c
opt_l = 0;
opt_a = 0;
/* traitement des options */
for (i = 1; i < argc; i++) {
    if (argv[i][0] == '-') {
        switch (argv[i][1]) {
        case 'l':
            opt_l = 1;
            break;
        case 'a':
            opt_a = 1;
            break;
        default:
            fprintf(stderr, "option inconnue ignorée '%c'\n", argv[i][1]);
        }
    }
}
/* traitement des arguments normaux */
for (i = 1; i < argc; i++) {
    if (argv[i][0] != '-') {
        /* traitement... */
    }
}

```

## Allons plus loin

Si le code précédent fonctionne, pourquoi utiliser une dépendance `getopt` ? Prenons une commande récente, permettant de trouver les utilisations de la fonction `assert` dans un code :
```sh
$ grep -C 4 -e 'assert(' -R -m 1 huffman
```
Cette commande donne les quatre lignes précédant et suivant la première utilisation `assert` dans les fichiers du dossier `huffman`. Elle se décompose ainsi (avec l'appui du manuel):

 - `-C 4`: Afficher 4 lignes de contexte. (avant et après)
 - `-e 'assert('`: Utiliser le `assert(` comme motif.
 - `-R`: Lire récursivement tous les fichiers à l'intérieur de chaque répertoire.
 - `-m 1`: Arrêter de lire un fichier après avoir trouvé 1 ligne sélectionnée.
 - `huffman`: nom d'un dossier (argument normal)

On remarque que certaines options (`C e m`) ont elles-mêmes un argument. Ainsi `4`, `assert(` et `1` ne sont pas des arguments normaux. Si on peut les gérer dans la première boucle, la seconde devient plus complexe à écrire.


```c
opt_C = 0;
opt_e = 0;
opt_R = 0;
opt_m = 0;
/* traitement des options */
for (i = 1; i < argc; i++) {
    if (argv[i][0] == '-') {
        switch (argv[i][1]) {
        case 'C':
            opt_C = 1;
            /* on suppose l'argument présent et correct */
            optarg_C = atoi(argv[i+1]);
            i++;
            break;
        case 'e':
            opt_e = 1;
            optarg_e = argv[i+1];
            i++;
            break;
        /* ... */
        }
    }
}
/* traitement des arguments normaux */
for (i = 1; i < argc; i++) {
    if (argv[i][0] == '-') {
        switch (argv[i][1]) {
        case 'C':
            /* on ignore l'argument de l'option */
            i++;
            break;
        case 'e':
            i++;
            break;
        /* ... */
        }
    } else {
        /* cas argument normal */
    }
}

```

À cela s'ajoute une autre particularité, rendant un peu plus complexe l'écriture de la première boucle, mais dont on trouve l'intérêt dans la ligne de commande : les options sans arguments peuvent se coller.

:::info
**Exemple :**
```shell
$ # -l et -a sont deux options
$ ls -l -a projet
$ # On peut les coller
$ ls -la projet

```
:::


## `getopt` à la rescousse

La fonction `getopt` permet l'analyse des arguments qui sont donnés à un programme en ligne de commande en islolant les options du reste des arguments. Elle est utilisé par plusieurs outils unix, unifiant le comportements de ces commandes.

```c
#include <unistd.h>

int getopt(int argc, char * const argv[], const char *optstring);
extern int optind;
extern char *optarg;
```

Ses deux premiers arguments sont ceux récupérés par la fonction `main`. Suit la description des options sous la forme d'une chaine de caractères.
Pour chaque option, la lettre de l'option doit faire partie de cette chaine de caractères. Si l'option prend elle-même un argument, un `:` est placé après la lettre.

:::info
**Exemple :**
Dans le cas des options de la commande `grep`, la description pourrait s'écrire `"RC:e:m:"`. L'ordre des options n'a pas d'importance.
:::

```c
/* traitement des options */
while ((opt = getopt(argc, argv, "RC:e:m:")) != -1) {
    switch (opt) {
    case 'C':
        opt_C = 1;
        /* on suppose l'argument correct, getopt garantit sa présence */
        /* optarg contient l'adresse de la chaine de caractères 
        contenant cet argument */
        optarg_C = atoi(optarg);
        i++;
        break;
    case 'e':
        opt_e = 1;
        optarg_e = optarg;
        i++;
        break;
    /* ... */
    }
}
/* Traitement des arguments normaux : getopt les déplacent à la suite des */
/* options, à partir de l'indice optind */
for (i = optind; i < argc; i++) {
    /* argv[i] */
}
```

## Cas du `--`

Quand il est présent dans les arguments, `getopt` stopera l'analyse au `--`, considérant ce qui suit comme des arguments normaux. Autrement dit, dans la ligne de commande `rm -- -r chose`, `-r` est considéré comme un argument normal, donc un fichier. 
Note: le premier `--` est alors omis par `getopt`.

## Aller plus loin avec le manuel

Le manuel indique plus d'information sur la gestion des erreurs, les arguments optionnels d'options, l'ordre de tous les arguments, les options longues avec `getopt_long`...
